RGU-design-OE-PCR

====

Overview

## Description

Efficient primer design program for Overlap-Extension PCR.

## Demo
Demonstration data is placed in tgtseq folder

## VS. 

## Requirement
OS: Ubuntu > 16.04
Ruby: Ruby > 2.5
Gem: csv (gem install csv)

Internet connection

## Installation
git clone git@bitbucket.org:dendoh_2/rgu-design-oepcr.git

## Preparation
gem install ./dnabitrue-0.3.0.gem

## Usage
Set target files in tgtseq folder with fasta-format.
Delete or move all files in order folder.
Execute oedesign.rb
($ ruby oedesign.rb)

## Contribution
Rakuno Gakuen University
Daiji Endoh
Gerry Amor Camer

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

Daiji Endoh

https://researchmap.jp/read0062406/

